
import sys
import os, os.path
from os.path import expanduser
from PIL import Image

#Define image folder path
DIR = expanduser("~") + "/results"
#Get all result image file paths and sort them on node. 0 is the left most image
imagesFiles = sorted([os.path.join(DIR, name) for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR, name))])
imagesFiles.sort(key=len)
#Open all files.
images = [Image.open(path) for path in imagesFiles]
#Get all sizes of the immages.
widths, heights = zip(*(i.size for i in images))
#Caclulate the total image width and height of all images.
total_width = sum(widths)
max_height = max(heights)
#Create new Image with calculated dimensions.
new_im = Image.new('RGB', (total_width, max_height))

#Write all images to the new created image.
x_offset = 0
for im in images:
    new_im.paste(im, (x_offset,0))
    x_offset += im.size[0]
#Save it to the file.
new_im.save('out_2000_transformed.png')
