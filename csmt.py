from __future__ import print_function
from pexpect import pxssh
from random import randint
from lxml import html
import getpass, sys, getopt, time, requests, re

# Usage
# python3 csmt.py -u r0623154 -l nodes.txt -n 2 -f format_commands.txt -s True
# python3 csmt.py -u r0623154 -l nodes.txt -n 2 -c commands.txt -s True

help = 'python csmt.py -u <username> -k <keyfile> -l <listfile> -n <nodecount> -c / -f <commandfile> -s <True/False>'
instanceId = str(randint(1000, 9999))
GlistFile = ""
Gscrape = False
def getScreenId(nodeId):
  return "csmt_" + str(instanceId) + "_Node" + str(nodeId)

def sendCommands(socket, commands, formatted, nodeId):
  # Process formatted commands, if required
  if formatted:
    tmpCommands = []
    for c in commands:
      tmpCommands.append(c.replace("%I", str(nodeId)))
    commands = tmpCommands
  # Generate a screenId unique across csmt instances & nodes
  screenId = getScreenId(nodeId)
  # Execute all supplied commands in a screen (add ';exec sh' to keep the screen open)
  cmd = 'screen -S ' + screenId + ' -dm bash -c "' + ';'.join(commands) + '"'
  print (cmd)
  socket.sendline (cmd)
  return screenId

def createSSHConnection(hostname, username, password, keyFile):
  socket = pxssh.pxssh()
  socket.login (hostname, username, password ,ssh_key=keyFile, sync_multiplier = 5)
  return socket

def scrapeNewNodes(listFile):
  # Update node listFile
  updateNodes(listFile)
  # Retrieve nodes from fresh listFile
  return sanitizeNodes(readFile(listFile))

def connectToNodes(username, password, keyFile, commands, nodes, maxConnections, formatted):
  busyNodes = []
  nodeId = 0
  hostId = 0
  print("Connecting to all nodes..")
  while (nodeId < maxConnections):
    try:
      # Connect to host
      hostname = nodes[hostId]
      print("Logging node " + str(nodeId) + " in on " + hostname + ": ..", end='')
      socket = createSSHConnection(hostname, username, password, keyFile)
      print("connected.. ", end='')
      # Send commands
      sendCommands(socket, commands, formatted, nodeId)
      socket.logout()
      print("commands delivered!")
      # Add node to list for later tracking
      busyNodes.append((nodeId,hostname))
      nodeId += 1
    except Exception as e:
      print("Failed: " + str(e))
    # Increment to the next host if not at the end
    if (hostId < len(nodes)-1):
      hostId += 1 
    else: 
      # if scraping enabled, refresh list & reset to node 0 (in case of scraping the best node)
      if Gscrape:
        nodes = scrapeNewNodes(GlistFile)
      hostId = 0
    # Wait a while to prevent triggering a ddos-prevention measure
    time.sleep(.5) #TODO: tweak parameter
  return busyNodes

# Connect a single node to another (functional) host 
def restartNode(nodeId, username, password, keyFile, commands, nodes, formatted):
  hostId = 0
  if Gscrape:
    nodes = scrapeNewNodes(GlistFile)
  while(True):
    try:
      # Connect to host
      hostname = nodes[hostId]
      socket = createSSHConnection(hostname, username, password, keyFile)
      # Send commands
      sendCommands(socket, commands, formatted,nodeId)
      socket.logout()
      print("Node " + str(nodeId) + " recovered.")
      # Succesfully recovered node, return tuple of node & new host
      return (node,nodes[hostId])
    except Exception as e:
      print("Restarting node " + str(nodeId) + " failed on " + nodes[hostId] + ".")
      # Retry on another host.
      if (hostId < len(nodes)-1):
        hostId += 1 
      else: 
        # If scraping is enabled, refresh list & reset to node 0 (in case of scraping the best node)
        if Gscrape:
          nodes = scrapeNewNodes(GlistFile)
        hostId = 0
        
def maintainNodes(busyNodes, username, password, keyFile, commands, nodes, formatted):
  print ("Entering monitoring mode, sleeping..", end = '')
  while(len(busyNodes) > 0):
    time.sleep(3) #TODO: tweak parameter
    print(" waking up & checking all nodes.")
    # Check all nodes
    for nodeTuple in busyNodes:
      (nodeId,hostname) = nodeTuple
      # Check availability of node (ssh & if screen is gone, check for decent shutdown)
      try: 
        socket = createSSHConnection(hostname, username, password, keyFile)
        # To detect a dead node, check presence in screen list & csmt status
        socket.send("screen -l;cat .csmt_status.log")
        #child.expect(pexpect.EOF)
        result = socket.before()
        if (result.find(getScreenId(nodeId)) == -1):
          # Node has died, restart it elsewhere
          hostname = restartNode(nodeId, username, password, keyFile, commands, nodes, formatted)
          # Save the new location of the node.
          nodeTuple = (nodeId,hostname)
      except Exception as e:
        # When connection to host fails, assume node is dead & restart it elsewhere
        hostname = restartNode(nodeId, username, password, keyFile, commands, nodes, formatted)
        # Save the new location of the node.
        nodeTuple = (nodeId,hostname)


def updateNodes(listFile):  
  print("Updating list of nodes..", end='')
  # Connect to the webpage
  url = "http://mysql.cs.kotnet.kuleuven.be/index.php?sort=load&display=uptime"
  page = requests.get(url)
  content = page.content
  print(".",end='')
  # Retrieve result
  tree = html.fromstring(content)
  nodes = tree.xpath('/html/body/center/pre/a/text()')
  connector = ".cs.kotnet.kuleuven.be"
  connectorNl = connector + '\n'
  reversed(nodes) # Because the site sorts from high load to low load
  # Remove bad nodes
  nodes.remove("one")
  nodes.remove("two")
  nodes.remove("odd")
  nodes.remove("even")
  # Write result to listFile
  with open(listFile, mode='wt', encoding='utf-8') as myfile:
      myfile.write(connectorNl.join(nodes[5:100])+connector)
  print (" done!")
    

def sanitizeCommands(dcommands): 
  commands = []
  idx = 0
  # Check commands..
  for c in dcommands:
    idx += 1
    if len(c.strip()) > 0:
      if c.find('"') > -1:
        print('A " has been detected in the command-file on line ' + str(idx) +  ', please remove or replace " with ' + "'"+ " and try again.")
        sys.exit(2)
      commands.append(c)
  return commands
  
def sanitizeNodes(dnodes):
  nodes = []
  # Check nodes..
  for n in dnodes:
    if len(n.strip()) > 0:
      nodes.append(n)
  return nodes

def getArguments(argv):
  # Retrieving arguments
  username = None
  keyFile = None
  listFile = None
  maxConnections = -1
  commandFile = None
  scrape = False
  formatted = False
# Define arguments
  shortP = "u:k:l:n:c:s:f:"
  longP = ["user=","key-file=","list-file=","nodes=","command-file=","formatted-command-file=","scrape="]
  try:
    opts, args = getopt.getopt(argv,shortP,longP)
  except getopt.GetoptError:
    print (help)
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print (help)
      sys.exit()
    elif opt in ("-u", "--user"):
      username = arg
      #print("setting username:" + arg)
    elif opt in ("-k", "--key-file"):
      keyFile = arg
      #print("setting keyFile:" + arg)
    elif opt in ("-l", "--list-file"):
      listFile = arg
      #print("setting listFile:" + arg)
    elif opt in ("-n", "--nodes"):
      maxConnections = int(arg)
      #print("setting maxConnections:" + arg)
    elif opt in ("-c", "--command-file"):
      commandFile = arg 
      #print("setting commandFile:" + arg)
    elif opt in ("-f", "--formatted-command-file"):
      commandFile = arg
      formatted = True
      #print("setting formatted-commandFile:" + arg) 
    elif opt in ("-s", "--scrape"):
      scrape = bool(arg)  
    else:
      print("Unknown param: " + opt + " with value: " + arg) 
  return [username,keyFile,listFile,maxConnections,commandFile,scrape,formatted]


def readFile(fileName):
  return [line.rstrip('\n') for line in open(fileName)]

def main(argv):

  # Retrieving arguments
  args = getArguments(argv)
  username = args[0]
  keyFile = args[1]
  listFile = args[2]
  maxConnections = args[3]
  commandFile = args[4]
  scrape = args[5]
  formatted = args[6]
  
  # Update the list of nodes, but only if we're ordered to
  if scrape:
    updateNodes(listFile)
  # Make the listFile & scrape globally accessible to enable continual scraping
  GlistFile = listFile
  Gscrape = scrape
  # Reading list-file & command-file
  dnodes = readFile(listFile)
  dcommands = readFile(commandFile)
  # Sanitize & error-check input
  nodes = sanitizeNodes(dnodes)
  commands = sanitizeCommands(dcommands)
  # Add bookkeeping command at the end, to denote completion of commands.
  commands.append("echo csmt_" + str(instanceId) + "_Node%I >> .csmt_status.log")
  print(commands)
  # Open up all connections
  password = getpass.getpass('Password: ')
  busyNodes = connectToNodes(username, password, keyFile, commands, nodes, maxConnections,formatted)
  # Keep track of nodes to guarantee availability
  #maintainNodes(busyNodes, username, password, keyFile, commands, nodes, formatted)

if __name__ == "__main__":
  if len(sys.argv) > 1:
    main(sys.argv[1:])
  else:
    print ("Please provide the following parameters:")
    print (help)
    sys.exit(2)
    
    
