This is the Computer Science Multithreader!
CSMT is a tool written in Python that enables you to spread a workload over hardware that's accessible through ssh.

Requirements (on remote machine): screen & cat

Usage:

python3 csmt.py -u \<username> -k \<keyfile> -l \<listfile> -n \<nodecount> -c \<commandfile> -s \<True/False>

Examples:

python3 csmt.py -u r0623154 -l nodes.txt -n 50 -f format_commands.txt -s True

python3 csmt.py -u r0623154 -l nodes.txt -n 8 -c commands.txt -s True


<dl>
  <dt>Options:</dt>
  <dd>-u, --username          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The user to use when connecting through SSH.</dd>
  <dd>-k, --key-file          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The key to use when authenticating through SSH.</dd>
  <dd>-l, --list-file         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A file containing a list of accessible nodes, one node per line.</dd>
  <dd>-n, --nodes             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The amount of nodes that will be connected to from the list in the same order. </dd>
  <dd>-c, --command-file      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A file containing commands that need to be executed on each node, one command per line. No " character allowed! Use ' instead.</dd>
  <dd>-l, --scrape            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This denotes whether or not it should update the node list-file  by scraping the hardware webpage (sorted by load). This is False by default.</dd>
  <dd>-f, --formatted-command-file           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A file containing formatted commands, which will replace any occurance of %I inside of the file with the index of the node it's running on, starting from 0. No " character allowed! Use ' instead. One command per line.
  </dd>
  
</dl>

**TODO**
*  Include built-in killall command for n nodes
*  Fix timestamps! Or do internal time keeping?
*  Guarantee availability of service (detect & restart dead nodes)
*  Attempt to secure passwords
*  Release commands from demands (Eg: no " symbols)
*  Include a cleanup/post-processing command-file
*  Extend image merger (realtime functionality)
*  include image merger in commandFactory
*  Provide tunnel to st.cs.kuleuven.be to ease development & use 
