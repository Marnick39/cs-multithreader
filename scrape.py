from lxml import html
import requests
import re

local = True # When False will retrieve data from url

#------ Use local file
if local: 
  localFile = "<localfile>"
  with open(localFile) as f:
      content = f.read()
#------ Use url
else: 
  url = "http://mysql.cs.kotnet.kuleuven.be/index.php?sort=load&display=uptime"
  page = requests.get(url)
  content = page.content
# Retrieve result
tree = html.fromstring(content)
nodes = tree.xpath('/html/body/center/pre/a/text()')
connector = ".cs.kotnet.kuleuven.be"
connectorNl = connector + '\n'
# Write result to disk
with open('nodes.txt', mode='wt', encoding='utf-8') as myfile:
    myfile.write(connectorNl.join(nodes[5:100])+connector)

