from __future__ import print_function
import sys, getopt

# Usage: python commandFactory.py -h 800 -w 800 -d 4 -c format_commands.txt
help = "python commandFactory.py -h <height> -w <width> -d <divisions> -c <filename>"
#try substracting one or adding one recursively to find closest common divider
def closestDivider(n,i):
  switch = True
  div = 0
  while(True):
    if (switch):
      if (n % (i-div)) ==0:
        return i - div
      switch = False
    else:
      if (n % (i+div)) ==0:
        return i + div
      switch = True
      div += 1
      
def main (argv):
  # Declare parameters
  height = -1
  width = -1
  div = -1
  commandFile = ""
  # Retrieve parameter values
  shortP = "h:w:d:c:"
  longP = ["height=","width=","divisions=","command-file="]
  try:
     opts, args = getopt.getopt(argv,shortP,longP)
  except getopt.GetoptError:
     print (help)
     sys.exit(2)
  for opt, arg in opts:
     if opt == '--help':
        print (help)
        sys.exit()
     elif opt in ("-h", "--height"):
        height = int(arg)
     elif opt in ("-w", "--width"):
        width = int(arg)
     elif opt in ("-d", "--divisions"):
        div = int(arg)
     elif opt in ("-c", "--command-file"):
        commandFile = arg
     else:
        print("Unknown param: " + opt + " with value: " + arg) 
  # Divide by 10 because of rendering engine limitations, 10p is the granularity of range.
  n = width / 10
  i = div
  # Calculate closest divider
  closestDiv = closestDivider(n,i)
  if i != closestDiv:
    print("Closest divider is " + str(closestDiv) + ", " + str(i) + " won't work.")
  # Preparing commands
  step = str(int(width / div))
  cmds = ["echo Node %I reports $(hostname) started work on $(date +'%D %H:%M:%S'). >> status.txt",
          "mkdir results",
          "./raytracer 1 ./results/render%I $(("+step+" * %I)) $(("+step+" * (%I  + 1))) 0 " + str(height),
          "echo 'Node %I reports' $(hostname) has finished on $(date +'%D %H:%M:%S'). >> status.txt"]
  print("Writing to disk...", end='')
  with open(commandFile, mode='wt', encoding='utf-8') as myfile:
      myfile.write('\n'.join(cmds))
  print (" Done!")
  

if __name__ == "__main__":
  if len(sys.argv) > 1:
    main(sys.argv[1:])
  else:
    print ("Please provide the following parameters:")
    print (help)
    sys.exit(2)
    
echo "$(date)";./raytracer_1 1 test.out 0 800 0 200  ;echo "$(date)"
